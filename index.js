// console.log("Hello World!");

// ARRAY METHODS

/*
	1. Mutator Methods
		- seeks to modify the contents of the array.
		-mutator methods are functions that mutate or change an array after they are created. These methods manipulate the original array performing various tasks such as adding or removing elements.
	
*/

let fruits = ["Apple", "Orange", "Kiwi", "Mango"];

/*
	push()
	 - adds an element at the end of an array and returns that array length.

	 Syntax:
	 	arrayName.push(element);
*/

console.log("Current Fruits Array:");
console.log(fruits);

//Adding element/s
let fruitsLength = fruits.push("Pineapple");
console.log(fruitsLength);
console.log("Mutated array using push method:");
console.log(fruits);


fruits.push("Avocado", "Guava");
console.log(fruits);


/*
	pop()
		- removes the last element in our array and returns the removed element (when the value is passed in another variable).

		Syntax:
			arrayName.pop();
*/

let removeFruit = fruits.pop();
console.log(removeFruit);
console.log("Mutated array using pop method:");
console.log(fruits);


/*
	unshift()
		- adds one or more elements at the beginning of an array and returns the length of the array (when its value is passed in another variable).

	Syntax:
		arrayName.unshift(element);
		arrayName.unshift(elementA, elementB, ...);
*/

let unshiftLength = fruits.unshift("Lime");
console.log(unshiftLength);
console.log("Mutated array using unshift method:");
console.log(fruits);

fruits.unshift("Banana","Strawberry");
console.log(fruits);


/*
	shift()
		- removes an element at the beginning of our array and returns the remove element when stored in a variable

	Syntax:
		- arrayName.shift();

*/

let removeShift = fruits.shift();
console.log(removeShift);
console.log("Mutated array using shift method:");
console.log(fruits);

fruits.shift();
console.log(fruits);



/*
	splice()
		- allows to simultaneouly remove elements from a specified index number and adds an element.

	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsTobeAdded);
*/

let fruitsSplice = fruits.splice(1, 2, "Cherry", "Dragon Fruit");
console.log(fruitsSplice);
console.log("Mutated array using splice method:");
console.log(fruits);

// using splice() without adding elements
fruits.splice(1, 1);
console.log(fruits);


/*
	sort()
		- rearranges the array elements in alphanumeric order.

	Syntax:
			arrayName.sort():
*/

fruits.sort()
console.log("Mutated array using sort method:");
console.log(fruits);

// ======================

let mixedArr = [50, 14, 100 ,1000, "Carlos", 79, "Niel", 53, "Seth", "Janie"];

console.log(mixedArr.sort());


/*
	reverse()
		- reverses the order of the elements in an array.

	Syntax:
		arrayName.reverse();

*/

fruits.reverse();
console.log("Mutated array using reverse method:");
console.log(fruits);


/*
// for sorting the item in descending order:
fruits.sort().reverse()
console.log(fruits);
*/

/*MINI ACTIVITY:
 - Debug the function which will allow us to list fruits in the fruits array.
 	-- this function should be able to receive a string.
 	-- determine if the input fruit name already exist in the fruits array.
 		*** If it does, show an alert message: "Fruit already listed on our inventory".
 		*** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
 	-- invoke and register a new fruit in the fruit array.
 	-- log the updated fruits array in the console


 	function registerFruit () {
 		let doesFruitExist = fruits.includes(fruitName);

 		if(doesFruitExst) {
 			alerat(fruitName "is already on our inventory")
 		} else {
 			fruits.push(fruitName);
 			break;
 			alert("Fruit is now listed in our inventory")
 		}
 	}
 	
*/

	
function registerFruit (fruitName) {
	let doesFruitExist = fruits.includes(fruitName);

	if(doesFruitExist) {
		alert(fruitName + " is already on our inventory");
	} else {
		fruits.push(fruitName);
		alert("Fruit is now listed in our inventory");
	};
};

// registerFruit("Kiwi"); //existing
// registerFruit("Grapes"); //new item
console.log(fruits);

/*
	2. Non-mutator Methods
	 	- these are functions or methods that do not modify or change an array after they are created. Theses methods also do not manipulate the original array but still perform various tasks such as returning elements from an array.
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
console.log(countries);


/*
	indexOf()
		- returns the index number of the first matching element found in an array. If no match was found, the result will be -1. The search process will be done from our first element proceeding to the last element.

	Syntax:
		arrayName.indexOf(searchvalue);
		arrayName.indexOf(searchValue, fromIndex);

*/


let firstIndex = countries.indexOf("PH");
console.log(firstIndex);
firstIndex=countries.indexOf("PH",4);
console.log("Result of indexOf method: " + firstIndex);
firstIndex=countries.indexOf("PH",7);
console.log("Result of indexOf method: " + firstIndex);
firstIndex=countries.indexOf("PH",-1); // -1 result because we should start from 0.
console.log("Result of indexOf method: " + firstIndex);

/*
	lastIndexOf()
		- returns the index umber of the last matching element found in an array. The search process will be done from the last element proceeding to the first element.

	Syntax:
		arrayName.lastIndexOf(searchvalue);
		arrayName.lastIndexOf(searchvalue, from Index);
*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf: " + lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf: " + lastIndexStart);

/*
	slice()
		- portion/slices elements from our array and return a new array.

	Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex,endingIndex);

*/

console.log(countries);
let slicedArrayA = countries.slice(2);
console.log("Result from slice method:");
console.log(slicedArrayA);
console.log(countries);

let slicedArrayB = countries.slice(0, 4);
console.log("Result from slice method:");
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log("Result from slice method:");
console.log(slicedArrayC);

/*
	toString()
		- returns an array as a string separated by commas.
		- is used internally in JS when an object need to be displayed as a text (like in HTML), or when an object needs to be used as a string.


		Syntax:
			arrayName.toString();

*/

let stringArray = countries.toString();
console.log("Result from toString method:");
console.log(stringArray);


/*
	concat()
		-combines two or more arrays and returns the combined result.

	Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let taskArrayA = ["drink HTML", "eat javascript"];
let taskArrayB = ["inhale css", "breath sass"];
let taskArrayC = ["get git", "be nodejs"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result of concat method:");
console.log(tasks);


//  Combining Multiple Arrays
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// Combining arrays with element - quite similar to push().
let combinedTasks = taskArrayA.concat("smell expressjs", "throw react");
console.log(combinedTasks);

/*
	join()
		- return an array as a string.
		- does not change the original array
		- any separator can be specified. The default separator is comma (,).

	Syntax:
		arrayName.join(separatorSymbol);
*/

let students = ["Rico","Michael","Paul","Levin"];
console.log(students);
console.log(students.join());
console.log(students.join(' '));
console.log(students.join(', '));
console.log(students.join(" - "));

/*
	3. Iteration Methods
		- are loops designed to perform repetitive tasks on arrays. this is useful for manipulating array data resulting in complex tasks.
		- normally works with a function, supplied as an argument
		- aims to evaluate each element in an array.
*/


/*
	forEach()
		- similar to "for loop" that iterates on each array element.

	Syntax:
		arrayName.forEach( function(individualelements){
			statement/business logic
		})
*/


allTasks.forEach(function(task){
	console.log(task);
});

// Using for each with conditional statements

let filteredTasks = [];

allTasks.forEach(function(task){
	console.log(task)

	if(task.length > 10){
		filteredTasks.push(task);
	};
});

console.log(allTasks);
console.log("Result of filteredTasks:");
console.log(filteredTasks);

/*
	map()
		- iterates on each elements and returns a new array with different values depending on the result of the function's operation.

	Syntax:
		let/const resultArray = arrayName.(function(individualelement){
			statemant;
		});

*/


let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function (number) {
	console.log(number);
	return number * number;
});

console.log("Orignal Array:");
console.log(numbers);
console.log("Result of map method:");
console.log(numberMap);


/*
	every()
		- checks if all elements in an array met the given condition. Return a "true" value if all elements meet the condition and "false" if otherwise.

	Syntax:
		let/const resultArray = arrayName.every(function(individualelement){
			return express/condition;
		});

*/

let allValid = numbers.every(function(number){
	return (number<6);
});


//let numbers = [1, 2, 3, 4, 5];
console.log("Result of every method:");
console.log(allValid);

/*
	some()
		- checks if at least one element in the array meet the given condition. Retruns a "true" value if at least one element meets the given condition and "false" if otherwise.

	Syntax:
		let/const resultArray = arrayName.some(function(individualelement){
			return express/condition;
		});

*/

let someValid = numbers.some(function(number){
	return (number<=1);
});

console.log("Result of some method:");
console.log(someValid);

/*
	filter()
		- returns a new array that contains element which meet the given condition. Returns an empty array if no element were found that satisfy the given condition.

	Syntax:
		let/const resultArray = arrayName.filter(function(individualelement){
			return express/condition;
		});	

*/

let filterValid = numbers.filter(function(number){
	return (number<3);
});


console.log("Result of filter method:");
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number == 0);
})

console.log("Result of filter method:");
console.log(nothingFound);



// Filtering using forEach()

let filterNumbers = [];

numbers.forEach(function(number){
	if(number < 4){
		filterNumbers.push(number)
	};
});

console.log("Result of filtering using forEach method:");
console.log(filterNumbers);

/*
	includes()
		- checks if the argument passed can be found in the array.

		- methods can be "chained" using them one after another. The result of the first method is being used on the second method until all the "chained" methods have been resolved.
*/

let products = ["Mouse", "Keyboard", "LAPTOP", "Monitor"];

let filteredProducts = products.filter(function(product){
		return product.toLowerCase().includes("a");
});


console.log("Result of includes method:");
console.log(filteredProducts);
